# Dépenses de défense
# On utilise les dépenses COFOG -> table gov_10a_exp
# Cette table est très détaillée -> gros volume

# Attention, dans les données en % du PIB, il y un arrondi à la décimale -> pas très précis
# -> on recalcule

rm(list=ls())

library(dplyr)
library(eurostat)
library(ggplot2)


# Importation table gov_10_exp --------------------------------------------
gov_10a_exp <- get_eurostat(id = "gov_10a_exp", time_format = "num")

# Modalités de la variable cofog99 ----------------------------------------
gov_10a_exp %>% 
  distinct(cofog99) %>% 
  left_join(
    get_eurostat_dic(dictname = "cofog99", lang="fr"),
    by = c("cofog99"="code_name")) %>% print(n=1e3)
# -> Défense= GF02 + détails


defense <- gov_10a_exp %>% 
  filter(substr(cofog99,1,4)=="GF02",
         sector == "S13",
         na_item == "TE",
         unit != "PC_GDP") %>% 
  select(-sector,-na_item)


# Ajouter dépense en part de PIB un peu plus précise ----------------------
nama_10_gdp <- get_eurostat(id = "nama_10_gdp", time_format = "num")

partPIB <- left_join(defense %>% filter(unit=="MIO_EUR"),
                     nama_10_gdp %>% filter(na_item == "B1GQ", unit=="CP_MEUR") %>% rename(PIB = values) %>% select(-unit,-na_item),
                     by = c("geo","time")
                     ) %>% 
  mutate(Part=values/PIB*100) %>%
  select(-values,-PIB) %>% 
  rename(values=Part) %>% 
  mutate(unit = "PC_GDP")


defense <- bind_rows(defense,partPIB)


# Comparaison du niveau des dépenses en 2018 ------------------------------

# On supprime les pays pour lesquels il y a une valeur manquante
def_tot2018 <- defense %>%
  filter(time == 2018,
         nchar(cofog99)==6,
         unit=="PC_GDP")

def_tot2018 %>% 
  anti_join(def_tot2018 %>% filter(is.na(values)) %>% group_by(geo) %>% slice(1) %>% ungroup,by="geo") %>% 
ggplot(aes(x = reorder(geo, values, sum),y=values, fill=cofog99))+
  geom_col() +
  scale_y_continuous(expand = expansion(mult=c(0,0.02))) +
  scale_fill_discrete(labels = c("GF0201" = "Défense militaire",
                                 "GF0202" = "Défense civile",
                                 "GF0203" = "Aide militaire à des pays étrangers",
                                 "GF0204" = "R&D défense",
                                 "GF0205" = "Autres défense"
                                 )) +
  theme(axis.ticks.x = element_blank(),
        legend.position = "top") +
  guides(fill = guide_legend(nrow=2)) +
  labs(x = "",
       y = "Part de PIB",
       fill =  "",
       title = "Dépenses de défense en 2018")


# Evolution des dépenses dans quelques pays -------------------------------
defense %>% 
  filter(cofog99=="GF02",
         geo %in% c("FR","DE","UK","ES","IT","PL"),
         unit == "PC_GDP") %>% 
  ggplot(aes(x=time,y=values,color=geo)) +
  geom_line(size=1) + geom_point() +
  labs(x = "",
       y = "Part de PIB",
       fill =  "",
       title = "Evolution des dépenses de défense totales")
