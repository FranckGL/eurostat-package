L'institut statistique européen [Eurostat](https://ec.europa.eu/eurostat/) met à disposition un très grand nombre de statistiques. Ces chiffres couvre un large champ : données économiques, démographiques, etc. Eurostat a créé un [package](https://cran.r-project.org/web/packages/eurostat/index.html) qui offre des fonctions d'importation des données directementdans R.

Les deux Rmd de ce repo présentent des utilisations de ce package :

* Le fichier 'Introduction.Rmd' est un tutoriel simple sur ce package;
* Le fichier 'Déambulation.Rmd' présente des cas d'utilisation de ces données, notamment sous forme graphique. (Attention, le tricottage du Rmd est assez long (5-10 minutes), car un volume assez important de données est récupéré sur le site d'Eurostat.)

Le répertoire 'Divers' contient des Rmd thématiques, sur les dépenses de protection sociale, le marché du travail, etc.

[Franck ARNAUD](mailto:franck.arnaud@ensae.org?subject=[GitLab]%20Package%20eurostat) - 21 décembre 2021 - Licence EUPL
